﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SW2
{
    public class Circle1
    {
        private static double radius = 1.0;
        private static string color  = "Red";

        //static void Circle()
        //{
        //    radius = 1.0;
        //    color = "Red";

        //}

        public void SetRadius(double NewValue)
        {
            radius = NewValue;

        }

        public void SetColor(string NewColor)
        {
            color = NewColor;

        }

        public double GetRadius()
        {
            return radius;
        }

        public string GetColor()
        {
            return color;
        }

    }



    /*public class Archer
    {
        public string name;
        public int hp;
        public int power;       

        public void move();
        public void basicattack();
        public void strafe();

    }*/



    class Program
    {
        static void Main(string[] args)
        {
            Circle1 C1 = new Circle1();

            Console.Write("Old Radius = ");
            Console.WriteLine(C1.GetRadius());

            Console.Write("Old Color = ");
            Console.WriteLine(C1.GetColor());

            Console.Write("Input new Radius: ");
            double x = Convert.ToDouble(Console.ReadLine());
            C1.SetRadius(x);

            Console.Write("Input new Color: ");
            string y = Convert.ToString(Console.ReadLine());
            C1.SetColor(y);
            
            Console.Write("New Radius = ");
            Console.WriteLine(C1.GetRadius());

            Console.Write("New Color = ");
            Console.WriteLine(C1.GetColor());

            Console.ReadKey();
        }
        
    }
}
