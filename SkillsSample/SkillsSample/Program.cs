﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkillsSample
{
    class Program
    {
        static void Main(string[] args)
        {
            BuffSkill bSkill = new BuffSkill("Defense Up");
            Skill skill = bSkill as Skill;
            skill.Use();
            Console.Read();
        }
    }
}
