﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkillsSample
{
    public class Skill
    {
        public string Name { get; set; }
        public Skill(string name)
        {
            Name = name;
        }

        public virtual void Use()
        {
            Console.WriteLine("Skill name: " + Name);
        }
    }
}
