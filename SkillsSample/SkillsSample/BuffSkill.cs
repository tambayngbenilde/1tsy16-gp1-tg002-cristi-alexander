﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkillsSample
{
    public class BuffSkill : Skill
    {
        public int StatIncrease;
        public string TargetStat;

        public BuffSkill(string name) : base(name)
        {

        }

        public override void Use()
        {
            base.Use();
            Console.WriteLine("Buff skill used for effect: " + TargetStat + ": " + StatIncrease + "!");
        }
    }
}
