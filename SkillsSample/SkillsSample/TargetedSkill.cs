﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkillsSample
{
    public class TargetedSkill : Skill
    {
        public int Damage;

        public TargetedSkill(string name) : base(name)
        {

        }

        public override void Use()
        {
            Console.WriteLine("Single target skill used for : " + Damage + " damage!");
        }
    }
}
