﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    public class Monster
    {
        private static Random random = new Random();

        // Constructor
        public Monster(string name, int hp, int acc, int xpReward,
            int armor, string weaponName, int lowDamage, int highDamage, int gold)
        {
            Name = name;
            hitPoints = hp;
            accuracy = acc;
            ExpReward = xpReward;
            Armor = armor;
            goldReward = gold;
            weapon = new Weapon(weaponName, lowDamage, highDamage);
        }
        
        public bool isDead { get { return hitPoints <= 0; } }
        public int ExpReward { get; private set; }
        public string Name { get; private set; }
        public int Armor { get; private set; }
        public int goldReward { get; private set; }


        public void Attack(Player playerTarget)
        {
            Console.WriteLine("A " + Name + " attacks you with a " + weapon.Name);

            if (RandomHelper.Random(0, 20) < accuracy)
            {
                int damage = RandomHelper.Random(weapon.DamageRange);
                int totalDamage = damage - playerTarget.Armor;

                if (totalDamage <= 0)
                {
                    Console.WriteLine("The monster's attack failed to penetrate your armor.");
                }
                else
                {
                    Console.WriteLine("You are hit for " + totalDamage + " damage!");
                    playerTarget.TakeDamage(totalDamage);
                }
            }
            else
            {
                Console.WriteLine("The " + Name + " missed!");
            }
            Console.WriteLine();
        }

        public void TakeDamage(int damage)
        {
            hitPoints -= damage;
            if (hitPoints < 0)
            {
                hitPoints = 0;
            }
        }

        public void DisplayHitPoints()
        {
            Console.WriteLine(Name + "'s hitpoints = " + hitPoints.ToString());
        }
        
        private int hitPoints;
        private int accuracy;
        
        private Weapon weapon;
    }
}
