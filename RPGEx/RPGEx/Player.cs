﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    public class Player
    {
        public string Name { get; private set; }

        // Constructor
        public Player()
        {//C:\Users\AJ\Documents\1tsy16-gp1-tg002-cristi-alexander\RPGEx\RPGEx\Player.cs
            Name        = "Default";
            className   = "Default";
            accuracy        = 0;
            hitPoints       = 0;
            maxHitPoints    = 0;
            mana            = 0;
            maxMana         = 0;
            gold            = 0;
            expPoints       = 0;
            nextLevelExp    = 0;
            level           = 0;
            Armor           = 0;
            weapon = new Weapon("Default Weapon Name", 0, 0);
            items = new List<Items>();

        }

        public void CreateClass()
        {
            Console.WriteLine("CHARACTER CLASS GENERATION");
            Console.WriteLine("==========================");

            // Input character's name.
            Console.WriteLine("Enter your character's name: ");
            Name = Console.ReadLine();

            Console.WriteLine("Please select a character class number...");
            Console.WriteLine("1) Fighter 2) Wizard 3) Cleric 4) Thief : ");

            int characterNum = 1;

            characterNum = Convert.ToInt32(Console.ReadLine());

            switch (characterNum)
            {
                case 1: //Fighter
                    className       = "Fighter";
                    accuracy        = 10;
                    hitPoints       = 20;
                    maxHitPoints    = 20;
                    mana            = 10;
                    maxMana         = 10;
                    expPoints       = 0;
                    nextLevelExp    = 1000;
                    level           = 1;
                    Armor           = 4;
                    weapon          = new Weapon("Long Sword", 1, 8);
                    break;
                case 2: //Wizard
                    className       = "Wizard";
                    accuracy        = 5;
                    hitPoints       = 10;
                    maxHitPoints    = 10;
                    mana            = 40;
                    maxMana         = 40;
                    expPoints       = 0;
                    nextLevelExp    = 1000;
                    level           = 1;
                    Armor           = 1;
                    weapon          = new Weapon("Staff", 1, 4);
                    break;
                case 3:
                    className       = "Cleric";
                    accuracy        = 8;
                    hitPoints       = 15;
                    maxHitPoints    = 15;
                    mana            = 30;
                    maxMana             = 30;
                    expPoints       = 0;
                    nextLevelExp    = 1000;
                    level           = 1;
                    Armor           = 3;
                    weapon = new Weapon("Flail", 1, 6);
                    break;
                default: //
                    className       = "Thief";
                    accuracy        = 7;
                    hitPoints       = 12;
                    maxHitPoints    = 12;
                    mana            = 15;
                    maxMana         = 15;
                    expPoints       = 0;
                    nextLevelExp    = 1000;
                    level           = 1;
                    Armor           = 2;
                    weapon          = new Weapon("Dagger", 1, 6);
                    break;
            }
        }

        public void ChooseRace()
        {
            Console.WriteLine("Choose a race :");
            Console.WriteLine("1) Human 2) Wildling 3) White Walker 4) High Valyrian");

            int PlayerRace = 0;

            PlayerRace = Convert.ToInt32(Console.ReadLine());

            switch (PlayerRace)
            {
                case 1: //Human - Balanced
                    accuracy        += 5;
                    maxHitPoints    += 5;
                    Armor           += 1;
                    maxMana         += 5;
                    race            = "Human";
                    break;

                case 2: //Wildling - Glass Cannon / Archer
                    accuracy        += 20;
                    maxHitPoints    -= 5;
                    Armor           += 3;
                    maxMana         += 10;
                    race            = "Wildling";
                    break;

                case 3: //White Walker - Tank
                    accuracy        += 10;
                    maxHitPoints    += 30;
                    Armor           -= 1;
                    maxMana         -= 10;
                    race            = "White Walker";
                    break;

                default: //High Valyrian - Knight
                    accuracy        += 10;
                    maxHitPoints    += 10;
                    Armor           += 5;
                    maxMana         -= 7;
                    race            = "High Valyrian";
                    break;


            }
            hitPoints   = maxHitPoints;
            mana        = maxMana;
        }

        public bool isDead { get { return hitPoints <= 0; } }
        public int Armor { get; private set; }
        public bool Attack(Monster monsterTarget)
        {
            int selection = 1;
            Console.Write("1) Attack, 2) Run, 3) Cast Spell, 4) Use Potion");
            selection = Convert.ToInt32(Console.ReadLine());
            switch (selection)
            {
                case 1:
                    Console.WriteLine("You attack an " + monsterTarget.Name + " with a " + weapon.Name);
                    if (RandomHelper.Random(0, 20) < accuracy)
                    {
                        int damage = RandomHelper.Random(weapon.DamageRange);
                        int totalDamage = damage - monsterTarget.Armor;
                        if (totalDamage <= 0)
                        {
                            Console.WriteLine("Your attack failed to penetrate the armor");
                        } else
                        {
                            Console.WriteLine("You attack for " + totalDamage.ToString() + " damage!");
                            monsterTarget.TakeDamage(totalDamage);
                        }
                    }
                    else
                    {
                        Console.WriteLine("You miss!");
                    }
                    break;
                case 2:
                    // 25% chance of being able to run
                    int roll = RandomHelper.Random(1, 4);
                    if (roll == 1)
                    {
                        Console.WriteLine("You run away!");
                        return true; //<-- Return out of the function
                    }
                    else
                    {
                        Console.WriteLine("You could not escape!");
                    }
                    break;

                case 3:
                    //spell menu
                    Spell Fireball = new Spell();
                    Fireball.Name = "Fireball";
                    Fireball.DamageRange.Low = 7;
                    Fireball.DamageRange.High = 10;
                    Fireball.MagicPointsRequired = 5;

                    Spell Thunderbolt = new Spell();
                    Thunderbolt.Name = "Thunderbolt";
                    Thunderbolt.DamageRange.Low = 15;
                    Thunderbolt.DamageRange.High = 20;
                    Thunderbolt.MagicPointsRequired = 20;

                    Spell LightningShock = new Spell();
                    LightningShock.Name = "Lightning Shock";
                    LightningShock.DamageRange.Low = 27;
                    LightningShock.DamageRange.High = 31;
                    LightningShock.MagicPointsRequired = 30;

                    Console.WriteLine("What Spell would you like to cast?");
                    Console.WriteLine("   NAME ---------- DAMAGE --------- MP COST");
                    Console.WriteLine("1. Fireball         7-10                  5   ");
                    Console.WriteLine("2. Thunderbolt      15-20                 20  ");
                    Console.WriteLine("3. Lightning Shock  27-31                 30  ");
                    int choice = Convert.ToInt32(Console.ReadLine());

                    switch (choice)
                    {
                        case 1:
                            if (mana >= Fireball.MagicPointsRequired)
                            {
                                Console.WriteLine("You chose " + Fireball.Name);
                                int SDmg = RandomHelper.Random(Fireball.DamageRange);
                                Console.WriteLine("You hit " + monsterTarget.Name + "for " + SDmg);
                                Console.WriteLine("You used " + Fireball.MagicPointsRequired);
                                Console.WriteLine("You now have " + mana + "mana left");
                                Console.WriteLine(" ");
                                monsterTarget.TakeDamage(SDmg);
                                mana -= Fireball.MagicPointsRequired;
                            }
                            else
                            {
                                Console.WriteLine("YOU DON'T HAVE ENOUGH MANA");
                                break;

                            }

                          break;

                        case 2:
                            if (mana >= Thunderbolt.MagicPointsRequired)
                            {
                                Console.WriteLine("You chose " + Thunderbolt.Name);
                                int SDmg = RandomHelper.Random(Thunderbolt.DamageRange);
                                Console.WriteLine("You hit " + monsterTarget.Name + "for " + SDmg);
                                Console.WriteLine("You used " + Thunderbolt.MagicPointsRequired);
                                Console.WriteLine("You now have " + mana + "mana left");
                                Console.WriteLine(" ");
                                monsterTarget.TakeDamage(SDmg);
                                mana -= Thunderbolt.MagicPointsRequired;
                            }
                            else
                            {
                                Console.WriteLine("YOU DON'T HAVE ENOUGH MANA");
                                break;

                            }

                            break;

                        case 3:
                            if (mana >= LightningShock.MagicPointsRequired)
                            {
                                Console.WriteLine("You chose " + LightningShock.Name);
                                int SDmg = RandomHelper.Random(LightningShock.DamageRange);
                                Console.WriteLine("You hit " + monsterTarget.Name + "for " + SDmg);
                                Console.WriteLine("You used " + LightningShock.MagicPointsRequired);
                                Console.WriteLine("You now have " + mana + "mana left");
                                Console.WriteLine(" ");
                                monsterTarget.TakeDamage(SDmg);
                                mana -= LightningShock.MagicPointsRequired;
                            }
                            else
                            {
                                Console.WriteLine("YOU DON'T HAVE ENOUGH MANA");
                                break;

                            }
                            break;

                        case 4:
                            Console.WriteLine("1. VIEW POTIONS | 2. USE POTION");
                            int select = Convert.ToInt32(Console.ReadLine());
                            switch (select)
                            {
                                case 1:
                                    for (int i = 0; i < items.Count; i++)
                                    {
                                        string name = i + " ) " + items[i].Name;

                                        Console.WriteLine();                                        
                                    }
                                    break;

                                case 2:
                                    if (hitPoints == maxHitPoints)
                                    {
                                        Console.WriteLine("You have full health");
                                        break;
                                    }

                                    else if (hitPoints < maxHitPoints)
                                    {
                                        Console.WriteLine("You used a Potion");
                                        hitPoints += items.First().HP;
                                        items.RemoveAt(1);
                                        break;
                                    }
                                    else if (items == null)
                                    {
                                        Console.WriteLine("You have no potions");
                                        break;
                                    }

                                    
                                    break;

                            }
                            break;
                    }
                    
                    break;

            }

            return false;
        }

        public void TakeDamage(int damage)
        {
            hitPoints -= damage;
            if (hitPoints < 0)
            {
                hitPoints = 0;
            }
        }

        public void LevelUp()
        {
            if (expPoints >= nextLevelExp)
            {
                Console.WriteLine("You gained a level!");

                // Increment level.
                level++;

                // Set experience points requard for next level
                nextLevelExp = level * level * 1000;

                // Increase stats randomly
                       
                switch (race)
                {
                    case "Human":
                        accuracy += RandomHelper.Random(2, 3);
                        maxHitPoints += RandomHelper.Random(2, 6);
                        maxMana += RandomHelper.Random(1, 5);
                        Armor += RandomHelper.Random(1, 2);
                        break;

                    case "Wildling":
                        accuracy += RandomHelper.Random(3, 6);
                        maxHitPoints += RandomHelper.Random(1, 4);
                        maxMana += RandomHelper.Random(3, 5);
                        Armor += RandomHelper.Random(2, 5);
                        break;

                    case "White Walker":
                        accuracy += RandomHelper.Random(1, 5);
                        maxHitPoints += RandomHelper.Random(5, 8);
                        maxMana += RandomHelper.Random(1, 3);
                        Armor += RandomHelper.Random(1, 3);
                        break;

                    case "High Valyrian":
                        accuracy += RandomHelper.Random(1, 7);
                        maxHitPoints += RandomHelper.Random(1, 3);
                        maxMana += RandomHelper.Random(4, 7);
                        Armor += RandomHelper.Random(3, 8);

                        break;

                }

                // Give the player full hitpoints when they level up.
                hitPoints = maxHitPoints;
            }
        }

        public void Rest()
        {
            
                Console.WriteLine("Resting...");
                Console.WriteLine("Restored HP");

                hitPoints = maxHitPoints;
                              

            // TODO: Modify the function so that random enemy enounters
            // are possible when resting

        }

        public void ViewStats()
        {
            Console.WriteLine("PLAYER STATS");
            Console.WriteLine("============");
            Console.WriteLine();

            Console.WriteLine("Name             = " + Name);
            Console.WriteLine("Class            = " + className);
            Console.WriteLine("Race             = " + race);   
            Console.WriteLine("Accuracy         = " + accuracy.ToString());
            Console.WriteLine("Hitpoints        = " + hitPoints.ToString());
            Console.WriteLine("MaxHitpoints     = " + maxHitPoints.ToString());
            Console.WriteLine("Mana             = " + mana.ToString());
            Console.WriteLine("Max Mana         = " + maxMana.ToString());
            Console.WriteLine("Current Gold     = " + gold.ToString());
            Console.WriteLine("XP               = " + expPoints.ToString());
            Console.WriteLine("XP to next level = " + nextLevelExp.ToString());
            Console.WriteLine("Level            = " + level.ToString());
            Console.WriteLine("Armor            = " + Armor.ToString());
            Console.WriteLine("Weapon Name      = " + weapon.Name);
            Console.WriteLine("Weapon Damage    = " + weapon.DamageRange.Low.ToString() + "-" + weapon.DamageRange.High.ToString());

            Console.WriteLine();
            Console.WriteLine("END PLAYER STATS");
            Console.WriteLine("================");
            Console.WriteLine();
        }

        public void Victory(int xp, int RewardGold)
        {
            Console.WriteLine("You won the battle!");
            Console.WriteLine("You got " + RewardGold.ToString() + " gold coins!");
            Console.WriteLine("You win " + xp.ToString() + " experience points!");
            expPoints += xp;
            gold += RewardGold;
        }

        public void GameOver()
        {
            Console.WriteLine("You died in battle...");
            Console.WriteLine();
            Console.WriteLine("==========================");
            Console.WriteLine("GAME OVER!");
            Console.WriteLine("==========================");
            Console.ReadLine();
        }

        public void DisplayHitPoints()
        {
            Console.WriteLine(Name + "'s hitpoints = " + hitPoints.ToString());
        }

        public void AddArmor(int Additional)
        {
            Armor += Additional;
        }

                              
        private string className;
        private string race;
        private int hitPoints;
        private int maxHitPoints;
        private int mana;
        private int maxMana;
        public int gold;
        private int expPoints;
        private int nextLevelExp;
        private int level;
        private int accuracy;
        private Weapon weapon;
        public List<Items> items; 
    }
}
