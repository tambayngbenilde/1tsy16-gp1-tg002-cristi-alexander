﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    class Store
    {


        public void EnterStore(Player player)
        {
            Console.WriteLine("Welcome to the store!");
            Console.WriteLine("What would you like to buy?");
            Console.WriteLine("1. ARMOR | 2. POTIONS");
            int choice = Convert.ToInt32(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    BuyArmor(player);
                    break;

                case 2:
                    Console.WriteLine(" ");
                    Console.WriteLine(" ");
                    BuyPotions(player);
                    break;                    
            }
        }

        public void BuyArmor(Player player)
        {
            Console.WriteLine("What armor would you like to buy?");
            Console.WriteLine(" ");
            Console.WriteLine("NAME ---------- DEFENSE VALUE ---------- PRICE");
            Console.WriteLine("Chainmail           10                     50 ");
            Console.WriteLine("Buckler             15                     100");
            Console.WriteLine("Blademail           30                     300");
            int choice = Convert.ToInt32(Console.ReadLine());
            switch (choice)
            {
                case 1:

                    Armor Chainmail     = new Armor();
                    Chainmail.Name      = "Chainmail";
                    Chainmail.Price     = 50;
                    Chainmail.Defense   = 10;

                    if (player.gold >= Chainmail.Price)
                    {
                        Console.WriteLine("You purchased " + Chainmail.Name + "for " + Chainmail.Price);
                        player.gold -= Chainmail.Price;
                        Console.WriteLine("Your gold is now " + player.gold);
                        player.AddArmor(Chainmail.Defense);
                        Console.WriteLine("Your armor is now " + player.Armor);

                    }
                    else
                    {
                        Console.WriteLine("YOU DON'T HAVE ENOUGH GOLD");
                        break;
                    }
                    break;

                case 2:

                    Armor Buckler = new Armor();
                    Buckler.Name = "Buckler";
                    Buckler.Price = 100;
                    Buckler.Defense = 15;

                    if (player.gold >= Buckler.Price)
                    {
                        Console.WriteLine("You purchased " + Buckler.Name + "for " + Buckler.Price);
                        player.gold -= Buckler.Price;
                        Console.WriteLine("Your gold is now " + player.gold);
                        player.AddArmor(Buckler.Defense);
                        Console.WriteLine("Your armor is now " + player.Armor);

                    } 
                    else
                    {
                        Console.WriteLine("YOU DON'T HAVE ENOUGH GOLD");
                        break;
                    }
                    break;

                case 3:

                    Armor Blademail = new Armor();
                    Blademail.Name = "Blademail";
                    Blademail.Price = 100;
                    Blademail.Defense = 15;

                    if (player.gold >= Blademail.Price)
                    {
                        Console.WriteLine("You purchased " + Blademail.Name + "for " + Blademail.Price);
                        player.gold -= Blademail.Price;
                        Console.WriteLine("Your gold is now " + player.gold);
                        player.AddArmor(Blademail.Defense);
                        Console.WriteLine("Your armor is now " + player.Armor);

                    }
                    else
                    {
                        Console.WriteLine("YOU DON'T HAVE ENOUGH GOLD");
                        break;
                    }
                    break;

            }


        }

        public void BuyPotions(Player player)
        {
            Console.WriteLine("HP potions cost 30 GOLD and heals for 40, would you like to buy one?");
            Console.WriteLine("1. YES 2. NO");
            int choice = Convert.ToInt32(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    if (player.gold >= 30)
                    {
                        Items Potion = new Items();
                        Potion.Name = "Salve";
                        Potion.HP = 40;
                        Potion.Price = 30;
                        player.items.Add(Potion);

                    }

                    else
                    {
                        Console.WriteLine("YOU DON'T HAVE ENOUGH MONEY");
                        break;
                    }
                    break;

            }


            
           

        }


    } 
}
