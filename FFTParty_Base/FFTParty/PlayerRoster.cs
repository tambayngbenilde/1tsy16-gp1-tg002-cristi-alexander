﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFTParty
{
    public class PlayerRoster
    {
        public int Gil { get; set; }
        public List<Character> Characters = new List<Character>();

        public void PrintRoster()
        {
            foreach (Character character in Characters)
            {
                Console.WriteLine("Printing Party");
                character.PrintMemberDetails();
            }
        }

        /// QUIZ ITEM
        /// Implement the EncounterMonster function, 
        /// this is when a Monster is about attack the party and potentially kill someone.
        /// The Monster's target can be read from monster.CharacterToKill
        /// Search your available party members for a match, if there's a match
        /// Remove that character from your list,
        /// If there isn't a match, just print "Monster wasn't able to kill anything!"
        /// Hint: Linear Search
        public void EncounterMonster(Monster monster)
        {
            Console.WriteLine("You have encountered an enemy " + monster.Name);
            
            //for (int i = 0; i < Characters.Count; i++)

            foreach (Character party in Characters)
            {
                if (party.JobName == monster.CharacterToKill)
                {
                    Console.WriteLine(" ");
                    Console.WriteLine(party.JobName + " was killed by " + monster.Name);
                    Console.WriteLine(" ");
                }

                else
                {
                    Console.WriteLine(" ");
                    Console.WriteLine(monster.Name + " wasn't able to kill anyone");
                    Console.WriteLine(" ");
                }


            }                             
            
            
        }

        /// QUIZ ITEM
        /// Compute the player's money earned
        /// Loop through each character and get their income
        /// Display the income and add it to the player's Gil
        public void ComputeEarnings()
        {
            foreach (Character character in Characters)
            {
                Console.WriteLine("Remaining Party");
                character.PrintMemberDetails();

                Console.WriteLine("You now have " + character.AddIncome(Gil) + "Gold.");
                Gil = character.AddIncome(Gil);

            }



        }
    }
}
