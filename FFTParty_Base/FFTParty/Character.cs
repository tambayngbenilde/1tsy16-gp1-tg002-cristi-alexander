﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFTParty
{
    public class Character
    {
        public int Id { get; private set; }
        public string Name { get; set; }
        public string JobName { get; private set; }
        public int RecruitmentCost { get; private set; }
        public int Income { get; private set; }

        public Character(int id, string jobName, int cost, int income)
        {
            Id = id;
            JobName = jobName;
            RecruitmentCost = cost;
            Income = income;
        }

        public void PrintRecruitmentDetails()
        {
            Console.WriteLine("Job: " + JobName + " | Cost:" + RecruitmentCost + " | Income: " + Income);
        }

        public void PrintMemberDetails()
        {
            Console.WriteLine("Name: " + Name + " | Job: " + JobName + " | Income: " + Income);
        }

        public int AddIncome(int gold)
        {
            gold += Income;
            return gold;
        }

    }
}
