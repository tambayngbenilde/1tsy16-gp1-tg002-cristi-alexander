﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public float speed;
    private Vector3 Def;
    private Rigidbody rb;

    // Use this for initialization
    void Start () {

        rb = GetComponent<Rigidbody>();
        Def = new Vector3 (1.11f, 1.11f, 1.11f);

    }
	
	// Update is called once per frame
	void FixedUpdate () {

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, moveVertical, 0.0f);

        rb.AddForce(movement * speed);


    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Reset"))
        {
            transform.Translate(Def);
        }
    }

}
