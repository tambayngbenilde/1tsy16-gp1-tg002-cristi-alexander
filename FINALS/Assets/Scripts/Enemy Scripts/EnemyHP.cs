﻿using UnityEngine;
using System.Collections;

public class EnemyHP : MonoBehaviour {

    public float HP;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
                	
	}

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "PlayerCannonball")
        {   
            HP--;
            Destroy(coll.gameObject);

            if (HP <= 0.0f){
                Destroy(gameObject);
            }

        }


    }

}
