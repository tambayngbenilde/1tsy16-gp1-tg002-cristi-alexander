﻿using UnityEngine;
using System.Collections;

public class SharkMovement : MonoBehaviour {

    private GameObject PlayerShip;
    public float speed;
    private Vector3 PlayerPos;
    private Vector3 TargetDirection;

    // Use this for initialization
    void Start () {

        PlayerShip = GameObject.FindGameObjectWithTag("Player");

        PlayerPos = PlayerShip.transform.position;

        

        TargetDirection = (PlayerPos - transform.position).normalized;

    }
	
	// Update is called once per frame
	void Update () {
        transform.position += TargetDirection * speed * Time.deltaTime;




    }
}
