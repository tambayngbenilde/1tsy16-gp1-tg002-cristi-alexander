﻿using UnityEngine;
using System.Collections;

public class LineRender : MonoBehaviour {
    public GameObject Xhair;  


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        GameObject Player = GameObject.FindGameObjectWithTag("Player");
        GetComponent<LineRenderer>().SetPosition(0, Xhair.transform.position);
        GetComponent<LineRenderer>().SetPosition(1, Player.transform.position);



    }
}
