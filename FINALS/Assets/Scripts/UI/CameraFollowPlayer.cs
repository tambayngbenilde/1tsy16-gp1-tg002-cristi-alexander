﻿using UnityEngine;
using System.Collections;

public class CameraFollowPlayer : MonoBehaviour {

    private float speed;
    private GameObject PlayerShip;
    private Vector3 Pos;

    // Use this for initialization
    void Start () {
        speed = 20.0f;      

	}
	
	// Update is called once per frame
	void Update () {
        Pos = transform.position;
        PlayerShip = GameObject.FindGameObjectWithTag("Player");
        Pos.z = PlayerShip.transform.position.z;      
        
        transform.position = Pos;
        transform.position = Vector3.MoveTowards(transform.position, PlayerShip.transform.position, speed * Time.deltaTime);
        
    }
}
