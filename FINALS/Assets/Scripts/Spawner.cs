﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

    private Vector3 position;

    public GameObject[] Enemy;
    //public GameObject Enemy;     

    private float def;
    public float SpawnRate;

	// Use this for initialization
	void Start () {

        //InvokeRepeating("Spawn", 2.0f, 2.0f);
        def = 0.0f;

        
	}
	
	// Update is called once per frame
	void Update () {
        bool canSpawn = false;
        
        def += Time.deltaTime;
        if (def > SpawnRate)
        {
            canSpawn = true;
            def = 0.0f;
        }
        if (canSpawn)
        {
            
            int rand = Random.Range(0, 100);
            if (rand <= 50)            
                position = GetXYPositivePosition();            

            else if (rand > 50)
                position = GetXYNegativePosition();


            int EnemyNum = GetEnemy();

            if (position != null)
                Spawn(position, EnemyNum);           
            
        }
      


	}

    int GetEnemy()
    {
        return Random.Range(0, Enemy.Length);
    }

    Vector3 GetNewPosition()
    {      
        return new Vector3(Random.Range(-60, 60) + transform.position.x, Random.Range(-60, 60) + transform.position.y, 0.0f);
    }

    Vector3 GetXYPositivePosition()
    {
        return new Vector3(Random.Range(60, 100) + transform.position.x, Random.Range(60, 100) + transform.position.y, 0.0f);
    }

    Vector3 GetXYNegativePosition()
    {
        return new Vector3(Random.Range(-60, -100) + transform.position.x, Random.Range(-60, -100) + transform.position.y, 0.0f);
    }

    void Spawn(Vector3 pos, int EnemNum)
    {
       Instantiate(Enemy[EnemNum], pos, transform.rotation);     
    }

}
