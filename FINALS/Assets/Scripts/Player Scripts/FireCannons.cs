﻿using UnityEngine;
using System.Collections;

public class FireCannons : MonoBehaviour {

       
    public GameObject Cannonball;
    private GameObject EnemyShip;
    private GameObject Player;

    

    public float FireRate;
    private float Rate;


	// Use this for initialization
	void Start () {

        Rate = 0.0f;

    }
	
	// Update is called once per frame
	void Update () {
        
        EnemyShip = GameObject.FindGameObjectWithTag("Enemy");

        bool CanFire = false;

        Rate += Time.deltaTime;

        if (Rate > FireRate)
        {
            CanFire = true;

            Rate = 0.0f;
        }

        if (EnemyShip != null)
        {
            if (CanFire)
            {
                Fire();
            }

        }

    }

    void Fire()
    {
        GameObject clone;
        clone = Instantiate(Cannonball, transform.position, EnemyShip.transform.rotation) as GameObject;
        clone.GetComponent<Rigidbody2D>().velocity = (EnemyShip.transform.position - transform.position);
    }
     

}
