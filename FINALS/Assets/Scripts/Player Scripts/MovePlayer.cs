﻿using UnityEngine;
using System.Collections;

public class MovePlayer : MonoBehaviour {
    private Vector3 MousePos;
    public float Speed;
	// Use this for initialization
	void Start () {        

	}
	
	// Update is called once per frame
	void Update () {

        MousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        MousePos.z = transform.position.z;
        transform.position = Vector3.MoveTowards(transform.position, MousePos, 50.0f * Time.deltaTime);


    }
}
