﻿using UnityEngine;
using System.Collections;

public class Crosshairs : MonoBehaviour {
    private Vector3 MousePos;   

    // Use this for initialization
    void Start () {
        

	
	}
	
	// Update is called once per frame
	void Update () {
        MousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        MousePos.z = transform.position.z;
        transform.position = Vector3.MoveTowards(transform.position, MousePos, 1000.0f * Time.deltaTime);

    }
}
