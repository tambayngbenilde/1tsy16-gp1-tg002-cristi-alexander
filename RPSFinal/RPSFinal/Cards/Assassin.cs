﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Assassin : Card
    {
        public override FightResult Fight(Card opponentCard)
        {
            //throw new NotImplementedException();
            if (opponentCard.Type is Warrior)
                return FightResult.Lose;

            else if (opponentCard.Type is Mage)
                return FightResult.Win;

            else
                return FightResult.Draw;

        }
    }
}
