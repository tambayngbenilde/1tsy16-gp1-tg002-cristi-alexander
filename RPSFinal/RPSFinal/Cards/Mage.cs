﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Mage : Card
    {
        public override FightResult Fight(Card opponentCard)
        {
            //throw new NotImplementedException();
            if (opponentCard.Type is Warrior)
                return FightResult.Win;

            else if (opponentCard.Type is Assassin)
                return FightResult.Lose;

            else
                return FightResult.Draw;

        }
    }
}
