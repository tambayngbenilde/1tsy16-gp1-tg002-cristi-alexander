﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Warrior : Card
    {
        public override FightResult Fight(Card opponentCard)
        {            
            //throw new NotImplementedException();
            if (opponentCard.Type is Assassin)
                return FightResult.Win;

            else if (opponentCard.Type is Mage)
                return FightResult.Lose;

            else
                return FightResult.Draw;

        }
    }
}
