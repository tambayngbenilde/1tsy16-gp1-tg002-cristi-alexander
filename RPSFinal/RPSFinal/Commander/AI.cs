﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class AI : Commander
    {
        /// <summary>
        /// 30% chance to discard, 70% chance to play a card.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            //throw new NotImplementedException();
            int rand = RandomHelper.Range(10);
            if (rand <= 70)
            {
                Console.WriteLine("The AI chose to play");

            }

            else
            {
                if (CanDiscard)
                    Discard();

            }



        }

        public override Card PlayCard()
        {
            throw new NotImplementedException();
        }
    }
}
