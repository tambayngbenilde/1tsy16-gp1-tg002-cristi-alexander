﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Player : Commander
    {
        /// <summary>
        /// Get player's input. Player can only either Play or Discard.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            //throw new NotImplementedException();
            DisplayCards();
            Console.WriteLine("Press 1 to Play");
            Console.WriteLine("Press 2 to Discard");
            int choice = Convert.ToInt32(Console.ReadLine());
            if (choice == 1)
            {
                Console.WriteLine("You chose to play against the AI");
                PlayCard();
                
                
                
            }
            else if (choice == 2)
            {
                Console.WriteLine("You chose to discard 2 cards to get a random unit");
                if (CanDiscard)
                {
                    Console.WriteLine("You have " + Cards.Count + "left");                
                    for (int i = 0; i > 2; i++)
                    {
                        int random = RandomHelper.Range(Cards.Count);
                        Cards.RemoveAt(random);
                        
                    }
                    Draw();               
                }
                else if (!CanDiscard)
                {
                    Console.WriteLine("You cannot discard right now");                 
                }

            }

        }

        /// <summary>
        /// Show a list of cards to choose from. If there are 2 cards of the same type (eg. Warrior), only show one of each type.
        /// </summary>
        /// <returns></returns>
        public override Card PlayCard()
        {
            //throw new NotImplementedException();
            DisplayCards();

            bool PlayWarrior  = false;
            bool PlayMage     = false;
            bool PlayAssassin = false;

            for (int i = 0; i < Cards.Count; i++)
            {
                if (Cards[i].GetType() is Warrior)
                {
                    Console.WriteLine("1. Warrior");
                }
            }

            for (int i = 0; i < Cards.Count; i++)
            {
                if (Cards[i].GetType() is Mage)
                {
                    Console.WriteLine("2. Mage");
                }
            }

            for (int i = 0; i < Cards.Count; i++)
            {
                if (Cards[i].GetType() is Assassin)
                {
                    Console.WriteLine("3. Assassin");
                }
            }
            
                
            Console.WriteLine(" ");
            Console.WriteLine("What card would you like to play?");
            int choice = Convert.ToInt32(Console.ReadLine());
            if (choice == 1)
            {
                PlayWarrior = true;
                for (int i = 0; i > Cards.Count; i++)
                {
                    if (Cards[i].Type is Warrior)
                    {
                        Cards.RemoveAt(i);
                    }
                }

            }

            else if (choice == 2)
            {
                PlayMage = true;
                for (int i = 0; i > Cards.Count; i++)
                {
                    if (Cards[i].Type is Mage)
                    {
                        Cards.RemoveAt(i);
                    }
                }

            }
            else
            {
                PlayAssassin = true;
                for (int i = 0; i > Cards.Count; i++)
                {
                    if (Cards[i].Type is Assassin)
                    {
                        Cards.RemoveAt(i);
                    }
                }

            }


            if (PlayWarrior)
                {
                    return new Warrior();
                }
            else if (PlayMage)
                {
                    return new Mage();
                }
            else
                {
                    return new Assassin();
                }

        }
    }
}
