﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory
{
    class Item
    {
        public string Name { get; set; }

        public int Price { get; set; }

        public Item(string name, int pricing)
        {
            Name = name;
            Price = pricing;

        }

        /*public enum Type
        {
            HP,
            MP,
            DAMAGE,

        }*/
        public virtual void Use()
        {
            Console.WriteLine("Skill name: " + Name);
        }

    }
}
