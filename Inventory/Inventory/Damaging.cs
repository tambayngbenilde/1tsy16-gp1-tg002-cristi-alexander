﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory
{
    class Damaging : Item
    {
        public int Damage;

        public Damaging(string name, int price, int dmg) : base (name, price)
        {
            Damage = dmg;

        }

        public override void Use()
        {
            base.Use();
            Console.WriteLine("You damaged an enemy for " + Damage + " with your " + Name);
        }

    }
}
