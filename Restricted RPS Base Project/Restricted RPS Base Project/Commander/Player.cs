﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Player : Commander
    {
        /// <summary>
        /// Get player's input. Player can only either Play or Discard.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            //throw new NotImplementedException();
            Console.WriteLine("Press 1 to Play");
            Console.WriteLine("Press 2 to Discard");
            int choice = Convert.ToInt32(Console.ReadLine());
            if (choice == 1)
            {
                Console.WriteLine("You chose to play against the AI");
                PlayCard();
            }
            else if (choice == 2)
            {
                Console.WriteLine("You chose to discard 2 cards to get a random unit");
                if (CanDiscard)
                {
                    Console.WriteLine("You have " + Cards.Count + "left");                
                    for (int i = 0; i > 2; i++)
                    {
                        int random = RandomHelper.Range(Cards.Count);
                        Cards.RemoveAt(random);
                        
                    }
                    int rand = RandomHelper.Range(3);
                    switch (rand)
                    {
                        case 0:
                            Cards.Add(new Warrior());
                            Console.WriteLine("You've drawn a new Warrior Card");
                            break;
                        case 1:
                            Cards.Add(new Mage());
                            Console.WriteLine("You've drawn a new Mage Card");
                            break;
                        case 2:
                            Cards.Add(new Assassin());
                            Console.WriteLine("You've drawn a new Assassin Card");
                            break;
                    }
                    
                    

                }
                else if (!CanDiscard)
                {
                    Console.WriteLine("You cannot discard right now");                 
                }

            }

        }

        /// <summary>
        /// Show a list of cards to choose from. If there are 2 cards of the same type (eg. Warrior), only show one of each type.
        /// </summary>
        /// <returns></returns>
        public override Card PlayCard()
        {
            //throw new NotImplementedException();
            DisplayCards();

            bool HasWarrior = false;
            bool HasMage = false;
            bool HasAssassin = false;

            for (int i = 0; i > Cards.Count; i++)
            {
                if (Cards[i] is Warrior)
                {
                    HasWarrior = true;
                }

                else if (Cards[i] is Mage)
                {
                    HasMage = true;
                }

                else if (Cards[i] is Assassin)
                {
                    HasAssassin = true;
                }

            }

            if (HasWarrior)
                Console.WriteLine("1. Warrior");
            


        }
    }
}
