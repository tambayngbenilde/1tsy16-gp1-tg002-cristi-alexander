﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResistanceBase
{
    class Program
    {
        static Random random = new Random();
        static int GetNumSpies(int totalPlayers)
        {
            // 5-6 players = 2 spies
            // 7-8 players = 3 spies
            // 9-10 players = 4 spies

            int NumofSpies;
            if (totalPlayers == 5 | totalPlayers == 6)
            {
                NumofSpies = 2;
                return NumofSpies;
            }

            else if (totalPlayers == 7 | totalPlayers == 8)
            {
                NumofSpies = 3;
                return NumofSpies;
            }

            else if (totalPlayers == 9 | totalPlayers == 10)
            {
                NumofSpies = 4;
                return NumofSpies;
            }

            throw new NotImplementedException();
        }

        static int EvaluateRound(string[] players)
        {
            // Return 0 if resistance wins
            // Return 1 if spies win
            throw new NotImplementedException();
        }


        static void PrintPlayers(string[] players)
        {
            for (int i = 0; i < players.Length; i++)
            {

                Console.WriteLine(players[i]);

            }



            //throw new NotImplementedException();
        }

        static void AssignRoles(string[] players)
        {

            // Must be dependent on the number of spies
            // Must always reach max # of spies. e.g. if there are 5 players there are always 2 spies
            // Must not exceed the # of spies for that set 
            // e.g if there are 7 players, there are always 3 spies, nothing less nothing more
            int x;
            x = GetNumSpies(players.Length);

            if (x == 2)
            {
                if (players.Length == 5)
                    string[] players = { "Spy", "Resistance", "Spy", "Resistance", "Resistance" };

                else
                    string[] players = { "Spy", "Resistance", "Spy", "Resistance", "Resistance", "Resistance",};
            }

            else if (x == 3)
            {



            }

            else if (x == 4)
            {



            }
            PrintPlayers(players);

            throw new NotImplementedException();
        }

        static int GetPlayersForMission(int totalPlayers)
        {
            // 5-6 players = 3 per mission
            // 7-8 players = 4 per mission
            // 9-10 players = 5 per mission
            throw new NotImplementedException();
        }

        static bool ValidateIndex(int[] missionPlayersIndex, int randomIndex)
        {
            for (int j = 0; j < missionPlayersIndex.Length; j++)
            {
                int presentIndex = missionPlayersIndex[j];
                if (presentIndex == randomIndex)
                {
                    return true;
                }
            }
            return false;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the Resistance!");

            int playersNum = 0;
            bool IncorrectNumPlayers = true;
            /////////////////////
            // QUIZ ITEM: Loop until the player enters a valid number
            /////////////////////

            while (IncorrectNumPlayers)
            {
                Console.Write("Enter number of players (5-10)");
                playersNum = Convert.ToInt32(Console.ReadLine());
                if (playersNum >= 5 && playersNum <= 10)
                {

                    IncorrectNumPlayers = false;
                }

                else
                {
                    Console.Write("Invalid Input");
                    Console.ReadKey();
                }
            }
            // END LOOP

            // Make an array of players based on the number the player entered
            string[] players = new string[playersNum];
            // Assign roles to the players
            AssignRoles(players);
            Console.Read();

            int resistanceScore = 0;
            int spiesScore = 0;
            int roundNum = 1;
            // Begin the missions
            while (resistanceScore < 3 && spiesScore < 3)
            {
                Console.Clear();
                Console.WriteLine("Mission " + roundNum.ToString());
                Console.WriteLine("Resistance: " + resistanceScore.ToString());
                Console.WriteLine("Spies: " + spiesScore.ToString());

                // Select random players based on the players array
                int missionPlayerCount = GetPlayersForMission(playersNum);
                List<int> missionPlayersIndex = new List<int>();
                string[] missionPlayers = new string[missionPlayerCount];
                for (int i = 0; i < missionPlayerCount; i++)
                {
                    int randomIndex = 0;
                    bool alreadyTaken = true;
                    while (alreadyTaken)
                    {
                        randomIndex = random.Next(0, players.Length);
                        alreadyTaken = ValidateIndex(missionPlayersIndex.ToArray(), randomIndex);
                    }
                    missionPlayersIndex.Add(randomIndex);
                    missionPlayers[i] = players[randomIndex];
                }
                // END select random players

                // Play the mission
                int result = EvaluateRound(missionPlayers);
                // Add the result score
                if (result == 0)
                {
                    resistanceScore++;
                }
                else
                {
                    spiesScore++;
                }
                roundNum++;
            }
            // END missions

            if (resistanceScore >= 3)
            {
                Console.WriteLine("Resistance Wins!");
            }
            else if (spiesScore >= 3)
            {
                Console.WriteLine("Spies Wins!");
            }
            Console.ReadKey();

        }
    }
}