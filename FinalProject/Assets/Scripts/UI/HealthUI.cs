﻿using UnityEngine;
using System.Collections;

public class HealthUI : MonoBehaviour
{

    public GameObject HP1;
    public GameObject HP2;
    public GameObject HP3;

    private float HP;

    // Use this for initialization
    void Start()
    {
        HP = 3.0f;
    }

    // Update is called once per frame
    void Update()
    {        
        
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "EnemyCannonball")
        {
            Debug.Log("LOST HP");
            HP--;

            if (HP == 2.0f)
                HP3.SetActive(false);

            if (HP == 1.0f)
                HP2.SetActive(false);

            if (HP == 0.0f)
                HP1.SetActive(false);
        }

    }

}