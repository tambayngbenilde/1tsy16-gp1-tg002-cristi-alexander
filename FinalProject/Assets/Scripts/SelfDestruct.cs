﻿using UnityEngine;
using System.Collections;

public class SelfDestruct : MonoBehaviour {
    public string target;
    
	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    void OnCollisionEnter2D(Collision2D collider)
    {
        if (collider.gameObject.tag == target)
        {
            Destroy(gameObject);

        }
    }

}
