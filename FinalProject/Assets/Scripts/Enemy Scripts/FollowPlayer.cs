﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {

    private GameObject PlayerShip;
    public float Speed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        PlayerShip = GameObject.FindGameObjectWithTag("Player");

        transform.position = Vector3.MoveTowards(transform.position, PlayerShip.transform.position, Speed * Time.deltaTime);
	}
}
