﻿using UnityEngine;
using System.Collections;

public class FireAtPlayer : MonoBehaviour {
    public GameObject EnemyProj;
    private GameObject PlayerShip;

    private float Rate;
    public float FireRate;



	// Use this for initialization
	void Start () {

        Rate = 0.0f;
        
    }

    // Update is called once per frame
    void Update() {
        bool CanFire = false;

        PlayerShip = GameObject.FindGameObjectWithTag("Player");

        Rate += Time.deltaTime;

        if (Rate > FireRate)
        {
            CanFire = true;         

            Rate = 0.0f;
        }

        if (CanFire)
        {
            Fire();
        }

    }

    void Fire()
    {
        GameObject clone;
        clone = Instantiate(EnemyProj, transform.position, PlayerShip.transform.rotation) as GameObject;     
        clone.GetComponent<Rigidbody2D>().velocity = (PlayerShip.transform.position - transform.position);        
    }


}
