﻿using UnityEngine;
using System.Collections;

public class EnemyBulletSelfDestruct : MonoBehaviour {

    private float TimeLimit;
    private float time;

	// Use this for initialization
	void Start () {

        TimeLimit = 3.0f;
        time = 0.0f;

	}
	
	// Update is called once per frame
	void Update () {
	   time += Time.deltaTime;


       if (time > TimeLimit)
       {
           Destroy(gameObject);

       }

	}
}
