﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory
{
    class Program
    {
        public static List<Item> Invent = new List<Item>();

        static void Main(string[] args)
        {
            Consumable Apple = new Consumable("Apple", 300, 15);
            Consumable Taiyaki = new Consumable("Taiyaki", 1100, 30);
            Consumable MixPizza = new Consumable("Mixed Pizza", 11000, 150);

            Buffing Ferromin = new Buffing("Ferromin", 1000, 3, "Health");
            Buffing FerrominFX = new Buffing("FerrominFX", 4000, 5, "Health");
            Buffing FerrominOme = new Buffing("Omega Ferromin", 30000, 12, "Health");

            Damaging WoodBow = new Damaging("Wooden Bow", 400, 7);
            Damaging LongSword = new Damaging("Longsword", 1200, 14);
            Damaging SilSpear = new Damaging("Silver Spear", 7200, 26);

            Invent.Add(Apple);
            Invent.Add(Taiyaki);
            Invent.Add(MixPizza);

            Invent.Add(Ferromin);
            Invent.Add(FerrominFX);
            Invent.Add(FerrominOme);

            Invent.Add(WoodBow);
            Invent.Add(LongSword);
            Invent.Add(SilSpear);

            
            Console.WriteLine("Would you like to view your inventory ");
            Console.WriteLine("Y/N");
            string select = Console.ReadLine();
            switch (select)
            {
                case "y":
                    Inventory.AddToInventory(Invent);

                    break;

                default:
                    Console.WriteLine("Quitting");
                    break;

            }



        }
    }
}
