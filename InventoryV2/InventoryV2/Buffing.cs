﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory
{
    class Buffing : Item
    {
        public int AmountBuffed;
        public string TargetStat;

        public Buffing(string name, int price,int amtbuffed, string targetstat) : base (name, price)
        {
            AmountBuffed = amtbuffed;
            TargetStat = targetstat;


        }
        public override void Use()
        {
            base.Use();
            Console.WriteLine("You buffed " + TargetStat + " for " + AmountBuffed);
        }

    }
}
