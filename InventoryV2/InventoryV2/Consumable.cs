﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory
{
    class Consumable : Item
    {
        public int Healing;

        public Consumable(string name, int price, int heal) : base(name, price)
        {
            Healing = heal;


        }

        public override void Use()
        {
            base.Use();
            Console.WriteLine("A Consumable was used," + Name + " healed for " + Healing);
        }

    }
}
